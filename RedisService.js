/**
 * kermit - soa infrastructure for node js
 *
 * @module      kermit-redis
 * @copyright   Copyright (c) 2016, Alrik Zachert
 * @license     https://gitlab.com/kermit-js/kermit/blob/master/LICENSE BSD-2-Clause
 */

'use strict';

const
  Service = require('kermit/Service'),
  redis   = require('redis');

class RedisService extends Service {
  constructor(serviceManager) {
    super(serviceManager);

    this.redisClient = null;
  }

  getRedisClient() {
    return this.redisClient;
  }

  getDefaultServiceConfig() {
    let env = process.env;

    return {
      host: env.REDIS_HOST,
      path: env.REDIS_PATH,
      port: env.REDIS_PORT,
      url:  env.REDIS_URL,

      auth: {
        password: env.REDIS_PASSWORD
      }
    };
  }

  launch() {
    this.redisClient = redis.createClient(
      this.serviceConfig.getHash()
    );

    if (this.serviceConfig.get('auth.password')) {
      this.redisClient.auth(
        this.serviceConfig.get('auth.password')
      );
    }

    ['ready', 'connect', 'reconnecting', 'error', 'end', 'drain', 'idle'].forEach(event => {
      this.redisClient.on(event, (arg) => this.emit(event, arg));
    });

    return this;
  }

  call(method) {
    let redisClient = this.redisClient;

    switch (arguments.length) {
      case 1:
        return redisClient[method]();
      case 2:
        return redisClient[method](arguments[1]);
      case 3:
        return redisClient[method](arguments[1], arguments[2]);
      case 4:
        return redisClient[method](arguments[1], arguments[2], arguments[3]);
      case 5:
        return redisClient[method](arguments[1], arguments[2], arguments[3], arguments[4]);
      case 6:
        return redisClient[method](arguments[1], arguments[2], arguments[3], arguments[4], arguments[5]);
      default:
        return redisClient[method].apply(redisClient, Array.prototype.slice.call(arguments, 1));
    }
  }
}

module.exports = RedisService;
